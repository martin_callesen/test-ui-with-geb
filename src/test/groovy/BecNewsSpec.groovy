import geb.spock.GebSpec

class BecNewsSpec extends GebSpec {
    def "Check header on news page 1"() {
        when: "You are on BEC news homepage"
        go "https://bec.dk/nyheder"

        then: "Check Page Header"
        $("h1", 1).text() == "Nyheder om BEC"
    }

    def "Check Number of news page 1"() {
        when: "You are on BEC news homepage"
        go "https://bec.dk/nyheder"

        then: "Check number of news"
        $("h4").findAll().size() == 24
    }

    def "Check Number of news page 2"() {
        when: "You are on BEC news homepage"
        go "https://www.bec.dk/nyheder/pages/2/"

        then: "Check number of news"
        $("h4").findAll().size() == 9
    }

    def "Access specific news page 2"() {
        when: "You are on BEC news homepage"
        go "https://www.bec.dk/nyheder/pages/2/"
        $("a", href:"https://www.bec.dk/bec-etablerer-nearshoring-setup-i-polen/", 0).click()
        waitFor { title.endsWith("BEC – finansvirksomhedernes it-partner")}

        then: "Check Page Header"
        $("h1", 0).text() == "BEC etablerer nearshoring-setup i Polen"
    }
}